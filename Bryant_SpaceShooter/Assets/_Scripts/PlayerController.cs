﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}
public class PlayerController : MonoBehaviour
{
    public float speed;
    public float tilt;
    public Boundary boundary;

    public GameObject shot;
    public Transform[] shotSpawns;
    public Transform shotSpawn;
    public float fireRate;

    public GameObject shield;
    public GameObject player;
    
    public Transform shieldSpawn;

    private float nextFire;

    private Rigidbody rb;
    private AudioSource audioSource;
    public bool TriFireOn = false;

    public float currentTime = 0f;
    public float TriFireTime = 5f;

    public bool ShieldOn = false;

    public GameObject pickupParticles;

    private GameController gameController;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        //shield.transform.SetParent(player) 

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }

         
    }
    void Update()
    {
        if (TriFireOn) //If the trifire ability is activated, start timer
        {
            TriFireTime -= 1 * Time.deltaTime;
            gameController.triFireTimerText.color = Color.yellow;
            gameController.triFireTimerText.text = "Tri-fire: " + TriFireTime.ToString("0") + "!";

          

            if (TriFireTime <= 0f) // if timer hits zero, turn off trifire
            {
                
                TriFireOn = false;
                gameController.triFireTimerText.text = "";
            }


        }

        
        

        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            //GameObject clone =
            if (TriFireOn == true) //if trifire is on, shoot from the three shot spawners
            {
                for (int i = 0; i < shotSpawns.Length; i++)
                {
                    Instantiate(shot, shotSpawns[i].position, shotSpawns[i].rotation); //Spot shots at each position
                }


            }
            else //if it's not, just shoot normally
            {
                Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            }
             // as GameObject;

            
            audioSource.Play();
        }

    }
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.velocity = movement * speed;
        rb.position = new Vector3
            (
            Mathf.Clamp (rb.position.x, boundary.xMin, boundary.xMax), 
            0.0f, 
            Mathf.Clamp (rb.position.z, boundary.zMin, boundary.zMax)
            );
        rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -tilt);

        
        
    }

    void OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.CompareTag("TriFire")) //if player collides with TriFire power up, they activate TriFire
        {
            TriFireOn = true;
            Destroy(other.gameObject);
            Instantiate(pickupParticles, transform.position, transform.rotation);
            TriFireTime = 5f;
            
            


        }

        if (other.gameObject.CompareTag("ShieldPickUp") && ShieldOn == false) // if player hits pick up with "ShieldPickUp" tag, they get the shield powerup
        {
            ShieldOn = true;
            Destroy(other.gameObject);
            Instantiate(pickupParticles, transform.position, transform.rotation);

            if (ShieldOn == true)
            {
                
              var myShield = Instantiate(shield, shieldSpawn.position, shieldSpawn.rotation); //spawn shield power up
                myShield.transform.parent = player.transform; //shield powerup moves with player

            
              

            }

            

        }

    }

    public void Die()
    {
        Destroy(gameObject);
    }
    
}
