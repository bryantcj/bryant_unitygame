﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScript : MonoBehaviour
{


    public int BossHealth = 15;
    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;
    private GameController gameController;

    // Start is called before the first frame update
    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (BossHealth <= 0f) //if boss has 0 health, the boss is destroyed, score is added, an explosion happens, the win condition is met, and the game can be restarted
        {
            Destroy(gameObject);
            gameController.YouWin();
            gameController.restartText.text = "Press 'R' to Restart!";
            gameController.restart = true;
            Instantiate(explosion, transform.position, transform.rotation);
            gameController.AddScore(scoreValue);

        }

        

    }




    void OnTriggerEnter(Collider other)
    {
        BossHealth -= 1; // if boss is shot, boss loses 1 health

        Instantiate(explosion, transform.position, transform.rotation);


        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            gameController.GameOver();
            gameController.restartText.text = "Press 'R' to Restart!";
            gameController.restart = true;

        }

        Destroy(other.gameObject);




    }

   
}
