﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpSpawner : MonoBehaviour
{
    // Start is called before the first frame update


    public GameObject[] pickupPrefabs;
    GameObject pickupSpawned;
    float waitToSpawn = 10f;
    float despawnTime;
    bool pickupPresent = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (pickupSpawned != null) // If there is a pickup on the screen
        {
            despawnTime -=Time.deltaTime;
            if (despawnTime <= 0f) //If the despawn timer gets to zero, destroy the spawned pick up(whatever it may be)
            {
                Destroy(pickupSpawned);
                 // set whether or not there is a pickup present to 'false'
            }
        }

        if (pickupSpawned ==null)//If pickup has been collected
        {
            if (waitToSpawn <= 0f)
            {
                SpawnPickup();//Spawn a new pickup
                waitToSpawn = 10f;
                
            }
            else
            {
                waitToSpawn -= Time.deltaTime;
            }
        }

        
       
        
    }


    void SpawnPickup()
    {

        GameObject pickupToSpawn = pickupPrefabs[(int)(Random.value * pickupPrefabs.Length)];
        pickupSpawned = Instantiate(pickupToSpawn, transform.position, pickupToSpawn.transform.rotation);
        
        despawnTime = 7f;
    }




}



