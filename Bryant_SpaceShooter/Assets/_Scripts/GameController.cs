﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public Text scoreText;
    public Text restartText;
    public Text gameOverText;
    public Text winText;
    public Text triFireTimerText;


    private bool gameOver;
    public bool restart;
    

    private int score;

    public GameObject boss;

    private PlayerController playerController;

    private void Start()
    {
        gameOver = false;
        restart = false;

        restartText.text = "";
        gameOverText.text = "";
        winText.text = "";
        triFireTimerText.text = "";
        score = 0;
        UpdateScore();
       StartCoroutine (SpawnWaves());




        GameObject playerControllerObject = GameObject.FindWithTag("Player");
        if (playerControllerObject != null)
        {
            playerController = playerControllerObject.GetComponent<PlayerController>();
        }

    }

    void Update()
    {
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                Application.LoadLevel(Application.loadedLevel);
            }

        }
        

    }



    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait); //there are now 'j' waves
        for (int j = 0; j < 3; j++) 
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

            if (gameOver)
            {
                restartText.text = "Press 'R' to Restart!";
                restart = true;
                break;
            }
        }
        //put spawn boss code here
        Vector3 spawnBossPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
        Quaternion spawnBossRotation = Quaternion.identity;
        Instantiate(boss, spawnBossPosition, spawnBossRotation);
            
    }

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }
    
    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }

    public void YouWin()
    {
        winText.text = "You Win!";
        gameOver = true;
    }
   // public void TriFireTimer()
    //{
       // if (playerController.TriFireOn) //If the trifire ability is activated, start timer
       // {
        //
       //     triFireTimerText.text = "TriFire: " + (playerController.TriFireTime);
      //      if (playerController.TriFireTime <= 0f) // if timer hits zero, turn off trifire
       //     {
      //          playerController.TriFireOn = false;
      //          triFireTimerText.text = "";
      //      }
  //      }
    //     
//
   // }
   //
    
}
