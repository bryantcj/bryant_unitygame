﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;
    private GameController gameController;
    private PlayerController playerController;
   // private BossScript bossScript;

   // public int bossHealth;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }

        GameObject playerControllerObject = GameObject.FindWithTag("Player");
        if (playerControllerObject != null)
        {
            playerController = playerControllerObject.GetComponent<PlayerController>();
        }

      //  GameObject bossScriptObject = GameObject.FindWithTag("Boss");
      //  if (bossScriptObject != null)
      //  {
      //      bossScript = bossScriptObject.GetComponent<BossScript>();
      //  }

        //bossHealth = 10;


    }

    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {



        if (other.tag == "Boundary" || other.CompareTag("Enemy") || other.CompareTag("TriFire") || other.CompareTag("ShieldPickUp")) //if the object has any of these tags, do nothing
        {
            return;
        }

       // if (explosion !=null && other.CompareTag("Boss"))
       // {
       //     return;
       // }

        if (explosion != null)
        {
            Instantiate(explosion, transform.position, transform.rotation);
        }
            

        if (other.tag == "Player") // if it's a player, destroy the player and end the game
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            playerController.Die();
            gameController.GameOver();
            
        }

        
        gameController.AddScore(scoreValue);
        Destroy(other.gameObject);
        
        Destroy(gameObject);


        if (other.tag == "Shield") //if other object is Shield, set the ShieldOn value to false and destroy the shield
        {
            Destroy(other.gameObject);
            playerController.ShieldOn = false;
            
        }

       // if (other.tag == "Boss")
       // {
       //     bossScript.OnTriggerEnter();
       // }


    }
}
