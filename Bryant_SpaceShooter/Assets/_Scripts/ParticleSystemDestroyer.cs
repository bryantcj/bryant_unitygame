﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemDestroyer : MonoBehaviour
{

    public float timeToDestroy;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyMe", timeToDestroy);
    }

    void DestroyMe()
    {
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
